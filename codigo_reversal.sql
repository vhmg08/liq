select codigo_banco, codigo_tipo_mensaje, sum(DECODE(CODIGO_TIPO_MENSAJE,'0210', CANTIDAD_SOLICITADA, '0420', CANTIDAD_BALANCE - CANTIDAD_SOLICITADA))
from red.transacciones_internacionales
where fecha_carga = to_date('100713','DDMMYY')
and codigo_banco = 'B032'
and ban_codigo_banco = 'MDS'
AND RAZON_RESPUESTA IN ('00', '01')                    
AND TIPO_TRANSACCION <> '21'                          
AND CLAVE_OPERACION = '10'
and codigo_reversal not in ('16')        
group by codigo_banco, codigo_tipo_mensaje;

