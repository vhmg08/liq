--################################################################################
--# Nombre del Programa : GRANTS_LIBRA.sql                                       #
--# Autor               : Victor H. Montoya G.                                   #
--# Compania            : eNova                                                  #
--# Proyecto/Procliente : P-02-0351-12                        Fecha: 09/05/2013  #
--# Descripcion General : permisos para usuarios                                 #
--# Programa Dependiente:                                                        #
--# Programa Subsecuente:                                                        #
--# Cond. de ejecucion  :                                                        #
--# Dias de ejecucion   :                                      Horario: hh:mm    #
--#                              MODIFICACIONES                                  #
--#------------------------------------------------------------------------------#
--# Autor               :                                                        #
--# Compania            :                                                        #
--# Proyecto/Procliente :                                      Fecha: dd/mm/yyyy #
--# Modificacion        :                                                        #
--#------------------------------------------------------------------------------#
--# Numero de Parametros:                                                        #
--# Parametros Entrada  :                                      Formato:          #
--# Parametros Salida   :                                      Formato:          #
--################################################################################

GRANT SELECT ON RED.TBL_CIFRAS_AMEX  TO SG;
GRANT SELECT ON RED.TBL_COMISIONES_AMEX   TO SG;





