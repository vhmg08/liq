/*
 ################################################################################
 # Nombre del Programa :  REDCLIQAMEXV03_01.pc                                  #
 # Autor               :  Victor H Montoya G                                    #
 # Compania            :  eNOVA                                                 #
 # Proyecto/Procliente :  P-20-0167-14                        Fecha: 20/09/2014 #
 # Descripcion General :  Calculo de volumen de tx                              #
 # Programa Dependiente:  N/A                                                   #
 # Programa Subsecuente:  N/A                                                   #
 # Cond. de ejecucion  :  N/A                                                   #
 # Dias de ejecucion   :  DIARIO                              Horario:02:00     #
 #                              MODIFICACIONES                                  #
 #------------------------------------------------------------------------------#
 # Autor               :                                                        #
 # Compania            :                                                        #
 # Proyecto/Procliente :                                   Fecha:  dd/mm/yyyy   #
 # Modificacion        :                                                        #
 #                                                                              #
 #------------------------------------------------------------------------------#
 # Ultima Actualizacion:                                                        #
 #------------------------------------------------------------------------------#
 # Numero de Parametros:  3                                                     #
 # Parametros Entrada  :  USUARIO,PASSWORD, FECHA             Formato:Alfanumer #
 # Parametros Salida   :  N/A                                 Formato: N/A      #
 # Archivos   Entrada  :  N/A                                                   #
 # Archivos   Salida   :                                                        #
 ################################################################################
 */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <setjmp.h>
#include <libgen.h>
#include <sqlcpr.h>
#include <sqlca.h>
#include <sqlda.h>
#include <oraca.h>

EXEC ORACLE OPTION (ORACA=YES);
EXEC ORACLE OPTION (HOLD_CURSOR=YES);
EXEC ORACLE OPTION (RELEASE_CURSOR=NO);
EXEC ORACLE OPTION (MAXOPENCURSORS=20);

EXEC SQL DECLARE db1 DATABASE;

extern SQLDA *sqlald();

SQLDA *binda;

/*VARIABLES GLOBALES PARA SQL*/
EXEC SQL BEGIN DECLARE SECTION;

char fecha_proc[9];
char fecha_primer[9];
char fecha_ultimo[9];

char cod_banco[5];
long cant;
double importe;
float comision;
float iva;
char tipo_com[2];

EXEC SQL END DECLARE SECTION;
/*----------------------*/
/*funciones*/
int abre_oracle( );
void desconecta_oracle();
void sql_error();
void calcula_fechas();
void conteo();
void busca_com();
void delete();

/*VARIABLES GLOBALES*/
varchar usuario[30];
char mm[3];
char yy[3];

/*----------------------*/

/*****************************************************************************
 RUTINA PRINCIPAL
 ******************************************************************************/
int main(int argc, char *argv[])
{
   char hora_ini[12],hora_fin[12];
   
   if (argc != 3 )
   {
      printf ("\nIngrese solo los parametros : %s <USER>/<PASSW> <YYYYMMDD> \n", argv[0]);
      exit (-1);
   }
   
   oraca.orastxtf = ORASTFERR;
   strcpy((char *)usuario.arr,argv[1]);
   usuario.len=strlen((char *)usuario.arr);
   usuario.arr[usuario.len]='\0';
   strcpy(fecha_proc,argv[2]);
   
   
   if (abre_oracle() != 0)
   {
      printf("No me pude conectar a Oracle\n");
      printf("%s\n",sqlca.sqlerrm.sqlerrmc);
      exit(1);
   }
   
   EXEC SQL AT db1 SELECT to_char(sysdate,'HH24:MI:SS')
   INTO :hora_ini
   FROM DUAL;
   
   printf("\nINICIO DE PROCESO %s [30Oct14:2155]-> %s\n",argv[0],hora_ini);
   printf("PARAMETROS:\n");
   printf("\tfecha_proc [%s]\n", fecha_proc);
   
   calcula_fechas();
   delete();
   conteo();
   
   EXEC SQL AT db1 SELECT to_char(sysdate,'HH24:MI:SS')
   INTO :hora_fin
   FROM DUAL;
   
   printf("\nREDCLIQAMEXV03_OK %s -> %s\n",argv[0],hora_fin);
   return(0);
}

/*calcula las fechas de incio y fin de mes*/
void calcula_fechas()
{  printf("\tcalcula_fechas():  ");
   memset(fecha_ultimo,NULL, sizeof(fecha_ultimo));
   memset(fecha_primer,NULL, sizeof(fecha_primer));
   memset(mm,NULL, sizeof(mm));
   memset(yy,NULL, sizeof(yy));
   
   /*mes*/
   strncpy(mm,fecha_proc+4,2);
   
   /*año*/
   strncpy(yy,fecha_proc+2,2);
   
   EXEC SQL SELECT TO_CHAR(TO_DATE(:fecha_proc,'YYYYMMDD') - 1,'YYYYMMDD')
   INTO :fecha_ultimo FROM DUAL ;
   printf("\tultimo dia [%s] ", fecha_ultimo);
   
   EXEC SQL SELECT TO_CHAR(TO_DATE(:fecha_proc,'YYYYMMDD') - 1,'YYYYMM') || '01'
   INTO :fecha_primer FROM DUAL ;
   
   printf("\tmes [%s] ", mm);
   printf("\taño [%s] ", yy);
   printf("\tprimer dia [%s] ", fecha_primer);
   printf("\tultimo dia [%s]/\n", fecha_ultimo);
}

void conteo(){
   printf("\tconteo(): ");
   EXEC SQL DECLARE CA CURSOR FOR
   SELECT CODIGO_BANCO, SUM(CANT) AS CANT, SUM(IMPORTE) AS IMPORTE FROM (
   SELECT CODIGO_BANCO, SUM(1) AS CANT,
   SUM(DECODE(CODIGO_TIPO_MENSAJE, '0210', CANTIDAD_SOLICITADA, '0420', CANTIDAD_BALANCE - CANTIDAD_SOLICITADA)) AS IMPORTE
   FROM TRANSACCIONES
   WHERE TRUNC(FH_ENTRADA_B24) BETWEEN TO_DATE(:fecha_primer,'YYYYMMDD') AND TO_DATE(:fecha_ultimo,'YYYYMMDD')
   AND CODIGO_TIPO_MENSAJE  ='0210'
   AND RAZON_RESPUESTA  IN ('00','01')
   AND TIPO_TRANSACCION <> '21'
   AND CLAVE_OPERACION = '10'
   AND BAN_CODIGO_BANCO = 'BAMX'
   GROUP BY CODIGO_BANCO
   UNION ALL
   SELECT CODIGO_BANCO, SUM(-1) AS CANT, SUM(CANTIDAD_BALANCE - CANTIDAD_SOLICITADA) AS IMPORTE
   FROM TRANSACCIONES
   WHERE TRUNC(FH_ENTRADA_B24) BETWEEN TO_DATE(:fecha_primer,'YYYYMMDD') AND TO_DATE(:fecha_ultimo,'YYYYMMDD')
   AND CODIGO_TIPO_MENSAJE = '0420'
   AND RAZON_RESPUESTA  IN ('00','01')
   AND TIPO_TRANSACCION <> '21'
   AND CLAVE_OPERACION = '10'
   AND BAN_CODIGO_BANCO = 'BAMX'
   GROUP BY CODIGO_BANCO
   UNION ALL
   SELECT CODIGO_BANCO, SUM(1) AS CANT, SUM(CANTIDAD_BALANCE) AS IMPORTE
   FROM TRANSACCIONES
   WHERE TRUNC(FH_ENTRADA_B24) BETWEEN TO_DATE(:fecha_primer,'YYYYMMDD') AND TO_DATE(:fecha_ultimo,'YYYYMMDD')
   AND CODIGO_TIPO_MENSAJE = '0420'
   AND CANTIDAD_BALANCE > 0
   AND RAZON_RESPUESTA  IN ('00','01')
   AND TIPO_TRANSACCION <> '21'
   AND CLAVE_OPERACION = '10'
   AND BAN_CODIGO_BANCO = 'BAMX'
   GROUP BY CODIGO_BANCO                                                                      )
   GROUP BY CODIGO_BANCO;
   
   EXEC SQL WHENEVER SQLERROR DO sql_error("Error: conteo\n");
	EXEC SQL OPEN CA;
	EXEC SQL WHENEVER NOT FOUND do break;
	
   while(1){
      EXEC SQL FETCH CA INTO :cod_banco, :cant, :importe;
      printf("\n\tfiid [%s] cant[%d]", cod_banco, cant);
      /*busca la comision a aplicar */
      busca_com();
      /*datos completos para insert de resumen*/
      EXEC SQL INSERT INTO TBL_RESUMEN_AMEX
      (MES, YYYY, CODIGO_BANCO, FECHA, CANTIDAD, IMPORTE, COMISION, IVA, TIPO_COM)
      VALUES(:mm,:yy,:cod_banco, sysdate, :cant, :importe, :comision, :iva, :tipo_com);
   }
   
   EXEC SQL CLOSE CA;
   
   EXEC SQL COMMIT;
   
   EXEC SQL WHENEVER NOT FOUND CONTINUE;
   printf("\t[%d] /\n", sqlca.sqlerrd[2]);
}

/*depura TBL_RESUMEN_AMEX del mes, yy actual*/
void delete(){
   printf("\tdelete()");
   EXEC SQL DELETE FROM TBL_RESUMEN_AMEX WHERE MES = :mm AND YYYY=:yy;
   EXEC SQL COMMIT;
   printf("\t DELETE TBL_RESUMEN_AMEX [%d]/\n", sqlca.sqlerrd[2]);
   
}
int abre_oracle()
{
   EXEC SQL CONNECT :usuario;
   return (sqlca.sqlcode);
}

void desconecta_oracle()
{
   EXEC SQL ROLLBACK WORK RELEASE;
}

/*busca la comision por volumen*/
void busca_com(){
   printf("\tbusca_com(): ");
   
   EXEC SQL DECLARE CC CURSOR FOR
   SELECT COMISION, IVA, TIPO_COM
   FROM TBL_COMISIONES_AMEX
   WHERE MINIMO <= :cant
   AND CODIGO_BANCO = :cod_banco
   ORDER BY MINIMO DESC;
   
   EXEC SQL WHENEVER SQLERROR DO sql_error("Error: busca_com\n");
   
	EXEC SQL OPEN CC;
	  
   EXEC SQL FETCH CC INTO :comision, :iva, :tipo_com;
   
   EXEC SQL CLOSE CC;
   
   EXEC SQL WHENEVER NOT FOUND CONTINUE;
   printf("fiid[%s],com[%f], tipo[%s] /\n", cod_banco, comision, tipo_com);
   
}

void sql_error(char *msg)
{
   EXEC SQL WHENEVER SQLERROR CONTINUE;
   printf("ERROR EN EL PROCEDIMIENTO->%s\n", msg);
   printf("\n\nERROR ORACLE:");
   sqlca.sqlerrm.sqlerrmc[sqlca.sqlerrm.sqlerrml] = '\0';
   oraca.orastxt.orastxtc[oraca.orastxt.orastxtl] = '\0';
   oraca.orasfnm.orasfnmc[oraca.orasfnm.orasfnml] = '\0';
   printf("\n%s\n", sqlca.sqlerrm.sqlerrmc);
   printf("in \"%s...\"\n", oraca.orastxt.orastxtc);
   printf("on line %d of %s.\n\n", oraca.oraslnr, oraca.orasfnm.orasfnmc);
   EXEC SQL ROLLBACK RELEASE;
   
   exit(-1);
}